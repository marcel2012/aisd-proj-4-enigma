# Enigma

Your task is to implement a variant of an Enigma machine which encrypts messages in a given alphabet A={a0, a1,...,an} of length n. An element of alphabet ai is called a letter and can consist of at most 128 of characters from the following set:

abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?

Rotors Wx={wx, 0, wx, 1,...,wx, n}and reflectors Ry={ry, 0, ry, 1,...,ry, n} simulate mechanical parts that can be used by an operator. A given permutation of the alphabet specifies wiring of the described rotor. This internal wiring does not change. When aligned with the alphabet passing through it is a simple substitution cipher ai->ri, however rotors can move if certain conditions are fulfilled (more information below).

Typing a letter into the input encrypts it by passing it through the rotors into reflector and back through the rotors in the inverse sequence, as seen here

Each rotor can shift counterclockwise by at most one step during each encoding. The position of a rotor is denoted by a letter - first letter of the alphabet corresponds to the position aligned with the alphabet as stated in the input, second after one rotation, etc. This encoding of the rotors position is used when specifing it's initial position and turnover letters. Refer to the scetion "Turnover notch positions" of the link and this section

The first rotor moves before every encoding. Therefore if the initial position of a single rotor is denoted by the last letter of the alphabet (see example I) the first letter is encoded according to substitutions as stated in the rotor's definition. If a rotor's position changes to one of its turnover letters, then the next rotor is advanced (e.g. if ai is a turnover letter of the first rotor then if the first rotor's position moves from ai-1 to ai, then the second rotor also moves). Note that this variant does not implement the double stepping feature of the historical machine.

If a letter or character (including whitespace characters) is not in the alphabet skip it. Character $ signals the end of a message to encode and is never a part of a letter.

Input can be tought as divided into two parts: definitions of pieces of the machine and p instructions how to encode a given message using some of those parts. Before encoding a message the machine is assembled from k out of m rotors and a single reflector (out of l) and the rotors are set to some initial positions. Refer to the input section for details.

### Input:
Defintions of parts of the machine:

    n- number of letters of the alphabet
    letters of the alphabet separated by whitespace characters
    m - number of rotors, followed by their definitions W0,...,Wm-1
        definition of rotor Wi - permutation of the alphabet
        number of letters which cause turnover, followed by these letters
    l - number of reflectors
    definitions of reflectors R0,...,Rl-1 - permutation of the alphabet


A set of p tasks to perform:

    p number of texts
    k - number of rotors in the machine
    k pairs - index of a rotor and its initial position (number of rotations prior to encoding)
    index of a reflector
    a sequence of letters (may contain whitespace characters) to encrypt, ending with the $ character

### Output:
encrypted sequence of letters (without the $ character) 