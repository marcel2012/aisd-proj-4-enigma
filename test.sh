for i in `seq 1 2`
do
	$1 < tests/$i.in > out.txt
	diff out.txt tests/$i.out
	echo "$i OK"
done
