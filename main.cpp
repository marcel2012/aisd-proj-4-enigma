#include <stdio.h>

#define MAX_LETTER_LENGTH 129
#define END_CHAR '$'
#define PRIME_NUMBER 101
#define PRIME_MODULO 1000003
#define PRIME_NUMBER2 97
#define PRIME_MODULO2 1000159
#define alphabetSizeToHashTableSize(n) (3*n)

struct letter {
    char data[MAX_LETTER_LENGTH];
    int hash, hash2;
};

struct list {
    list *next = NULL;
    const letter *data = NULL;
    int idInAlphabet;

    list(const letter *data, list *next, const int &idInAlphabet) {
        this->data = data;
        this->next = next;
        this->idInAlphabet = idInAlphabet;
    }
};

inline void addToHash(letter &el, const char &c) {
    el.hash = (el.hash * PRIME_NUMBER + c) % PRIME_MODULO;
    el.hash2 = (el.hash2 * PRIME_NUMBER2 + c) % PRIME_MODULO2;
}

inline void hash(letter *el) {
    const char *str = el->data;
    el->hash = el->hash2 = 0;
    while (*str) {
        addToHash(*el, *str);
        str++;
    }
}

struct rotor {
    int *permutation;
    int *reversedPermutation;
    bool *isTurnoverLetter;
    int turnoverSize;
};

struct reflector {
    int *permutation;
};

inline bool equals(const letter &a, const letter &b) {
    if (a.hash2 != b.hash2)
        return false;
    const char *aStr = a.data, *bStr = b.data;
    while (*aStr && *bStr) {
        if (*aStr != *bStr)
            return false;
        aStr++;
        bStr++;
    }
    return !(*aStr || *bStr);
}

inline void addElementToList(list **hashTable, const letter *data, const int size, const int idInAlphabet) {
    const int id = data->hash % alphabetSizeToHashTableSize(size);
    list *el = new list(data, hashTable[id], idInAlphabet);
    hashTable[id] = el;
}

inline void removeElementFromList(list **hashTable, const int &id) {
    list *el = hashTable[id];
    hashTable[id] = hashTable[id]->next;
    delete el;
}

inline int indexOf(list **hashTable, const int &size, const letter &el) {
    const int id = el.hash % alphabetSizeToHashTableSize(size);
    const list *listEl = hashTable[id];
    while (listEl != NULL) {
        if (equals(*(listEl->data), el))
            return listEl->idInAlphabet;
        listEl = listEl->next;
    }
    return -1;
}

inline int normalize(const int &i, const int &n) {
    return (i < 0 ? i + n : i % n);
}

inline void encrypt(const letter *alphabet, const int &n, int &positionIndex, rotor *rotors[], int *rotorStates,
                    const int &rotorsSize,
                    const reflector &r, const bool &moveRotor) {
    if (rotorsSize) {
        rotor *nowRotor = rotors[0];
        int &nowState = *rotorStates;
        bool moveNext = false;
        if (moveRotor) {
            nowState = normalize(nowState + 1, n);
            moveNext = nowRotor->isTurnoverLetter[nowState];
        }
        positionIndex = normalize(positionIndex + nowState, n);
        positionIndex = nowRotor->permutation[positionIndex];
        positionIndex = normalize(positionIndex - nowState, n);
        encrypt(alphabet, n, positionIndex, rotors + 1, rotorStates + 1, rotorsSize - 1, r, moveNext);
        positionIndex = normalize(positionIndex + nowState, n);
        positionIndex = nowRotor->reversedPermutation[positionIndex];
        positionIndex = normalize(positionIndex - nowState, n);
    } else
        positionIndex = r.permutation[positionIndex];
}

char buffor[1000];
int bufforIndex = 0;

inline char getNextChar() {
    if (!buffor[bufforIndex]) {
        scanf("%999s", buffor);
        bufforIndex = 0;
    }
    return buffor[bufforIndex++];
}

inline void readLetter(letter *l) {
    scanf("%s", l->data);
    hash(l);
}

int main() {
    int n;
    letter tmpLetter;
    scanf("%d", &n);
    letter *alphabet = new letter[n];
    list **hashTable = new list *[alphabetSizeToHashTableSize(n)];
    for (int i = 0; i < alphabetSizeToHashTableSize(n); i++)
        hashTable[i] = NULL;
    for (int i = 0; i < n; i++) {
        readLetter(alphabet + i);
        addElementToList(hashTable, alphabet + i, n, i);
    }
    int m;
    scanf("%d", &m);
    rotor *rotors = new rotor[m];
    for (int i = 0; i < m; i++) {
        rotors[i].permutation = new int[n];
        rotors[i].reversedPermutation = new int[n];
        for (int j = 0; j < n; j++) {
            readLetter(&tmpLetter);
            const int index = indexOf(hashTable, n, tmpLetter);
            rotors[i].permutation[j] = index;
            rotors[i].reversedPermutation[index] = j;
        }
        scanf("%d", &(rotors[i].turnoverSize));
        rotors[i].isTurnoverLetter = new bool[n]();
        for (int j = 0; j < rotors[i].turnoverSize; j++) {
            readLetter(&tmpLetter);
            const int index = indexOf(hashTable, n, tmpLetter);
            rotors[i].isTurnoverLetter[index] = true;
        }
    }
    int l;
    scanf("%d", &l);
    reflector *reflectors = new reflector[l];
    for (int i = 0; i < l; i++) {
        reflectors[i].permutation = new int[n];
        for (int j = 0; j < n; j++) {
            readLetter(&tmpLetter);
            const int index = indexOf(hashTable, n, tmpLetter);
            reflectors[i].permutation[j] = index;
        }
    }

    int p;
    scanf("%d", &p);
    while (p--) {
        int k;
        scanf("%d", &k);
        rotor **selectedRotors = new rotor *[k];
        int *rotorStates = new int[k];
        for (int i = 0; i < k; i++) {
            int index;
            scanf("%d", &index);
            readLetter(&tmpLetter);
            const int initState = indexOf(hashTable, n, tmpLetter);
            selectedRotors[i] = rotors + index;
            rotorStates[i] = initState;
        }
        int reflectorIndex;
        scanf("%d\n", &reflectorIndex);
        char *charToRead = tmpLetter.data;
        tmpLetter.hash = tmpLetter.hash2 = 0;
        while (true) {
            *charToRead = getNextChar();
            if (*charToRead == END_CHAR) {
                buffor[0] = 0;
                bufforIndex = 0;
                break;
            }
            addToHash(tmpLetter, *charToRead);
            charToRead++;
            *charToRead = 0;
            int index = indexOf(hashTable, n, tmpLetter);
            if (index != -1) {
                encrypt(alphabet, n, index, selectedRotors, rotorStates, k, reflectors[reflectorIndex], true);
                printf("%s", alphabet[index].data);
                charToRead = tmpLetter.data;
                tmpLetter.hash = tmpLetter.hash2 = 0;
            }
        }
        printf("\n");
        delete[] selectedRotors;
        delete[] rotorStates;
    }
    delete[] alphabet;
    for (int i = 0; i < m; i++) {
        delete[] rotors[i].permutation;
        delete[] rotors[i].reversedPermutation;
        delete[] rotors[i].isTurnoverLetter;
    }
    for (int i = 0; i < alphabetSizeToHashTableSize(n); i++)
        while (hashTable[i] != NULL)
            removeElementFromList(hashTable, i);
    delete[] hashTable;
    delete[] rotors;
    for (int i = 0; i < l; i++)
        delete[] reflectors[i].permutation;
    delete[] reflectors;
    return 0;
}